///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    catDatabase.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    10 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include "config.h"

void initializeDatabase()
{
	for(int i = 0; i < MAX_CATS; i++)
	{
		cats[i].name[0] = 0;
		cats[i].weight = 0.0;
	}
	
	totalCats = 0;
	
}
